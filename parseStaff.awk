#!/usr/bin/awk -f
BEGIN {
	print "Title\tName\tStatus\tOffice\tCell\tE-mail"
	# Initialize variables:
	title = ""
	name = ""
	status = ""
	office = ""
	cell = ""
	email = ""
	# Set output field separator to use tab for column separator
	OFS = "\t"
}

# On each line:
{
	if($1 == "Name") {
		name = substr($0, 6)
		# See if name ends in "(Mrs)" and set title to "Mrs." if required
		if((startOfMrs = index(name, "(Mrs)")) > 0) {
			title = "Mrs."
			# Remove (Mrs) from end of name
			name = substr(name, 1, startOfMrs-2)
		}
	}
	if($1 == "Status") {
		status = substr($0, 8)
		# See if status is "Professor" or "Associate Professor" and set title to "Prof."
		if(status ~ /Professor/) title = "Prof."
	}
	if($1 == "Office") office = substr($0, 8)
	if($1 == "Cell") cell = substr($0, 6)
	if($1 == "E-mail") email = substr($0, 8)
	
	
	# Going on to a new block?
	if($0 == "") {
		# Trim trailing whitespace from variables, if they exist.
		sub(/[ \t]+$/, "", name)
		sub(/[ \t]+$/, "", status)
		sub(/[ \t]+$/, "", office)
		sub(/[ \t]+$/, "", cell)
		sub(/[ \t]+$/, "", email)
		#If we've found at least a name, print out a line with all information.
		if(name != "") {
			print title, name, status, office, cell, email
		}
		title = ""
		name = ""
		status = ""
		office = ""
		cell = ""
		email = ""
	}
}

END {
}